﻿using Banregio.Clases;
using System;

namespace Banregio
{
    class Program
    {
        static void Main(string[] args)
        {
            Banco banco = new Banco();
            banco.Procesar();
        }
    }
}
