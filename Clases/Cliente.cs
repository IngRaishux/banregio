﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Banregio.Clases
{
    class Cliente
    {
        private double monto = 0;
        public string Nombre { get; set; }
        public double Monto
        {
            get {
                return monto;
            }
        }

        public void Depositar( double cantidad)
        {
            monto = monto + cantidad;
            Console.WriteLine($"El monto actual de {Nombre} es: {Monto}");
        }

        public void Retirar(double cantidad)
        {
            monto = monto - cantidad;
            Console.WriteLine($"El monto actual de {Nombre} es: {Monto}");
        }
    }
}
